# Changelog

## v1.2.2

- Fixed navigation bar for SearXNG
- Added two SearXNG instances

## v1.2.1

- Fixed panel footer in sidebar

## v1.2.0

- Search results page:
	- Styled border of time and language dropdowns
	- Styled instant answer

## v1.1.0

- Styled warning “Engines cannot retrieve results”
- Styled image previews and thumbnails
- Options added:
	- Specify custom colour value for warning just mentioned
	- Change between two predefined colours for (most) borders
	- Specify custom border colour

## v1.0.0

Initial release
