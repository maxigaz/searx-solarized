# Searx Solarized

A [Usercss](https://github.com/openstyles/stylus/wiki/Usercss) theme for [Searx](https://asciimoo.github.io/searx/), using the colour palette from [Solarized](https://ethanschoonover.com/solarized/) (plus one additional colour for borders).

## Screenshots

![Main page (Solarized Dark)](Screenshot-main-dark.png)
![Main page (Solarized Light)](Screenshot-main-light.png)
![Results page (Solarized Dark)](Screenshot-results-dark.png)
![Results page (Solarized Light)](Screenshot-results-light.png)

## Install

1. Get a style manager. The style has been tested with [Stylus](https://add0n.com/stylus.html), although it is also compatible with [xStyle](https://github.com/FirefoxBar/xStyle). Other style managers might also work but are *not* supported.
2. Install the stylesheet by clicking on one of the mirror links below and a new window of Stylus should open, asking you to confirm to install the style.
3. In Searx, under *Preferences*, select the theme *oscar* and the style *Logicodev*. (This is usually the default setting, although it may differ between instances.)

Mirrors:

- Codeberg: [Install directly with Stylus](https://codeberg.org/maxigaz/searx-solarized/raw/master/searx-solarized.user.css)
- GitLab: [Install directly with Stylus](https://gitlab.com/maxigaz/searx-solarized/raw/master/searx-solarized.user.css)

## Options

Currently, the following options are available in case you want to customise the style:

- Switch between Solarized Dark and Solarized Light (affects the background and text colours)
- Choose between two predefined border colours or provide your own value
- Provide a custom colour value for the background of warning messages
- **Provide your own domain** for any Searx instance (by default, only four instance domains are set)

## Reporting issues

In the issue description, include the following:

- An example URL to the page you’re experiencing the problem on or provide step by step instructions on how to reproduce it.
- The version of your web browser, style manager, and the userstyle.
- In addition, including a screenshot or screencast of the problem is also helpful.

## Changelog

Changes between releases are summarized [here on Codeberg](https://codeberg.org/maxigaz/searx-solarized/src/branch/master/CHANGELOG.md) and [here on GitLab](https://gitlab.com/maxigaz/searx-solarized/blob/master/CHANGELOG.md).

## Credits

In almost all cases, the colour palette from [Solarized](https://ethanschoonover.com/solarized/) has been followed as an example.

I learned some useful conventions–particularly on variables—by studying the userstyles created by [Raitaro Hikami](https://gitlab.com/RaitaroH). Thanks a lot!

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here](https://gitlab.com/maxigaz/userstyles).
